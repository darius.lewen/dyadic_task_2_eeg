import pyglet

from misc import conf

conf.screens = [0]
conf.eye_tracking = True
# conf.aim_radius = .05

from replay.replayengine import ReplayEngine
from ui.ui import UI
from data_loader import print_pair_idx_to_ids


if __name__ == '__main__':
    print_pair_idx_to_ids()
    # engine = ReplayEngine(pair_idx=36, lap=1)  # Phi=1.00 comp
    # engine = ReplayEngine(pair_idx=38, lap=1)  # Phi=0.00 coop nearest
    # engine = ReplayEngine(pair_idx=51, lap=1)  # Phi=0.39
    engine = ReplayEngine(pair_idx=8, lap=1)   # Phi=0.33

    # engine = ReplayEngine(pair_idx=55, lap=1)  # Phi=0.00 coop tit4tat
    # engine = ReplayEngine(pair_idx=53, lap=1)  # Phi=0.99 optimal free roaming placement

    gaze_collectors = engine.gazes if conf.eye_tracking else None
    ui = UI(engine, gaze_collectors=gaze_collectors, replay=True)

    pyglet.app.run()

    # 0[100, 101]0.19649122807017544
    # 1[102, 103]0.8684654300168634
    # 2[104, 105]0.06726907630522089
    # 3[200, 201]0.5261865793780688
    # 4[202, 203]0.3684210526315789
    # 5[204, 205]0.30358705161854765
    # 6[206, 207]0.4832925835370823
    # 7[208, 209]0.23917322834645668
    # 8[210, 211]0.33276450511945393
    # 9[300, 301]1.0
    # 10[302, 303]1.0
    # 11[304, 305]0.14827890556045895
    # 12[306, 307]0.32839721254355403
    # 13[308, 309]0.6534810126582279
    # 14[310, 311]1.0
    # 15[400, 401]0.2938634399308557
    # 16[404, 405]0.9827160493827161
    # 17[406, 407]0.3016260162601626
    # 18[408, 409]0.0691588785046729
    # 19[410, 411]0.0423572744014733
    # 20[412, 413]0.5989583333333334
    # 21[414, 415]0.409435551811289
    # 22[416, 417]1.0
    # 23[418, 419]0.07268951194184839
    # 24[422, 423]0.31774960380348655
    # 25[424, 425]1.0
    # 26[426, 427]0.20189003436426117
    # 27[428, 429]0.8662207357859532
    # 28[430, 431]0.5601291364003228
    # 29[432, 433]1.0
    # 30[434, 435]0.0054446460980036296
    # 31[436, 437]0.4568487727632621
    # 32[438, 439]0.9584332533972821
    # 33[440, 441]0.10820559062218214
    # 34[442, 443]0.9992006394884093
    # 35[444, 445]0.015458937198067632
    # 36[446, 447]1.0
    # 37[448, 449]0.3803827751196172
    # 38[450, 451]0.0
    # 39[454, 455]0.8106557377049181
    # 40[456, 457]0.1389144434222631
    # 41[460, 461]0.18
    # 42[462, 463]0.7180762852404643
    # 43[464, 465]0.13108614232209737
    # 44[466, 467]0.9753086419753086
    # 45[468, 469]0.9788851351351351
    # 46[470, 471]0.3235831809872029
    # 47[472, 473]0.22157996146435452
    # 48[474, 475]0.23893065998329155
    # 49[476, 477]0.99921875
    # 50[478, 479]0.2603648424543947
    # 51[480, 481]0.3944396177237185
    # 52[482, 483]0.36647493837304845
    # 53[486, 487]0.9992354740061162
    # 54[488, 489]0.002824858757062147
    # 55[490, 491]0.0
    # 56[492, 493]0.15678776290630975
    # 57[494, 495]0.0009970089730807576

    # engine = ReplayEngine(pair_idx=31, lap=1)  # 304, 305 coop tit4tat
    # engine = ReplayEngine(pair_idx=51, lap=1)  # effort 2 prev t invite - 0.25fst
    # engine = ReplayEngine(pair_idx=45, lap=1)  # 462 .75fst

    # engine = ReplayEngine(pair_idx=21, lap=1)  # 412 .75fst they seem to fight a liddle

    # engine = ReplayEngine(pair_idx=13, lap=1)  # 308, 309 distri

    # engine = ReplayEngine(pair_idx=15, lap=1)  # 400
    # engine = ReplayEngine(pair_idx=18, lap=1)  # 406

    # engine = ReplayEngine(pair_idx=7, lap=0)  # 208
    # engine = ReplayEngine(pair_idx=8, lap=0)  # 210



    # engine = ReplayEngine(pair_idx=8, lap=1)  # 210, 211

    # engine = ReplayEngine(pair_idx=35, lap=1)  # 442, 443 nice comp
    # engine = ReplayEngine(pair_idx=59, lap=1)  # 304, 305 coop tit4tat
    # engine = ReplayEngine(pair_idx=61, lap=1)  # 304, 305 coop nearest

    # engine = ReplayEngine(pair_idx=46, lap=1)  # 464, 465 effort 3 prev t invite - 0.02fst
    # engine = ReplayEngine(pair_idx=45, lap=1)  # effort 2 prev t invite - 0.75fst

    # engine = ReplayEngine(pair_idx=3, lap=1)  # 200, 201 distance min
    # engine = ReplayEngine(pair_idx=1, lap=0)
    # engine = ReplayEngine(pair_idx=16, lap=0)  # 402, 403 comp
    # engine = ReplayEngine(pair_idx=24, lap=0)  # 418, 419 comp
    # engine = ReplayEngine(pair_idx=0, lap=1)
    # engine = ReplayEngine(pair_idx=5, lap=1)
    # engine = ReplayEngine(pair_idx=26, lap=1)
    # engine = ReplayEngine(pair_idx=11, lap=1)  # 304, 305  coop
    # engine = ReplayEngine(pair_idx=7, lap=1)  # 208, 209 nice to show inv
    # engine = ReplayEngine(pair_idx=8, lap=0)  # 210, 211
    # engine = ReplayEngine(pair_idx=24, lap=0)  # 418, 419 comp  # many declined invites lead to competition
    # engine = ReplayEngine(pair_idx=11, lap=1)  # 304, 305  coop

    # engine = ReplayEngine(pair_idx=39, lap=1)  # 304, 305 coop nearest
    # engine = ReplayEngine(pair_idx=61, lap=1)  # 304, 305 coop nearest
    # engine = ReplayEngine(pair_idx=58, lap=1)  # 304, 305 coop tit4tat orange seams to want nearest but blue is dominant
    # engine = ReplayEngine(pair_idx=31, lap=1)  # 304, 305 coop tit4tat
    # engine = ReplayEngine(pair_idx=36, lap=1)  # 304, 305 coop
    # engine = ReplayEngine(pair_idx=20, lap=1)  # 304, 305 coop
    # engine = ReplayEngine(pair_idx=2, lap=1)  # 304, 305 coop
    # engine = ReplayEngine(pair_idx=19, lap=1)  # 304, 305 coop
    # engine = ReplayEngine(pair_idx=24, lap=1)  # 304, 305 coop
    # engine = ReplayEngine(pair_idx=34, lap=1)  # 304, 305 coop


# 0.0 [490 491] 59
# 0.0 [450 451] 39
# 0.0009970089730807576 [494 495] 61
# 0.002824858757062147 [488 489] 58
# 0.0054446460980036296 [434 435] 31
# 0.015458937198067632 [444 445] 36
# 0.0423572744014733 [410 411] 20
# 0.06726907630522089 [104 105] 2
# 0.0691588785046729 [408 409] 19
# 0.07268951194184839 [418 419] 24
# 0.10820559062218214 [440 441] 34
# 0.13108614232209737 [464 465]
# 0.1389144434222631 [456 457]
# 0.14827890556045895 [304 305]
# 0.15678776290630975 [492 493]
# 0.18 [460 461]
# 0.19649122807017544 [100 101]
# 0.20189003436426117 [426 427]
# 0.22157996146435452 [472 473]
# 0.23893065998329155 [474 475]
# 0.23917322834645668 [208 209]
# 0.2603648424543947 [478 479]
# 0.2938634399308557 [400 401]
# 0.3016260162601626 [406 407]
# 0.30358705161854765 [204 205]
# 0.31774960380348655 [422 423]
# 0.3235831809872029 [470 471]
# 0.32839721254355403 [306 307]
# 0.33276450511945393 [210 211]
# 0.36647493837304845 [482 483]
# 0.3684210526315789 [202 203]
# 0.3803827751196172 [448 449]
# 0.3944396177237185 [480 481]
# 0.409435551811289 [414 415]
# 0.4568487727632621 [436 437]
# 0.4832925835370823 [206 207]
# 0.5261865793780688 [200 201]
# 0.5601291364003228 [430 431]
# 0.5989583333333334 [412 413]
# 0.6534810126582279 [308 309]
# 0.7180762852404643
# 0.8106557377049181 [454 455]
# 0.8662207357859532 [428 429]
# 0.8684654300168634 [102 103]
# 0.9584332533972821 [438 439]
# 0.9753086419753086 [466 467]
# 0.9788851351351351 [468 469]
# 0.9827160493827161 [404 405]
# 0.9992006394884093 [442 443]  aaa
# 0.99921875 [476 477]
# 0.9992354740061162 [486 487]
# 1.0 [432 433]
# 1.0 [416 417]
# 1.0 [446 447]
# 1.0 [302 303]
# 1.0 [310 311]
# 1.0 [300 301]
# 1.0 [424 425]
# pair_idx: 0 p_ids: [100, 101] record_numbers: [3, 8]
# pair_idx: 1 p_ids: [102, 103] record_numbers: [3, 4, 5]
# pair_idx: 2 p_ids: [104, 105] record_numbers: [3, 4, 5]
# pair_idx: 3 p_ids: [200, 201] record_numbers: [4, 5, 7]
# pair_idx: 4 p_ids: [202, 203] record_numbers: [4, 5, 6]
# pair_idx: 5 p_ids: [204, 205] record_numbers: [2, 3, 4]
# pair_idx: 6 p_ids: [206, 207] record_numbers: [3, 5]
# pair_idx: 7 p_ids: [208, 209] record_numbers: [4, 6, 7]
# pair_idx: 8 p_ids: [210, 211] record_numbers: [8, 9]
# pair_idx: 9 p_ids: [300, 301] record_numbers: [4, 5, 6]
# pair_idx: 10 p_ids: [302, 303] record_numbers: [2, 3, 4]
# pair_idx: 11 p_ids: [304, 305] record_numbers: [3, 4, 5]
# pair_idx: 12 p_ids: [306, 307] record_numbers: [3, 4, 5]
# pair_idx: 13 p_ids: [308, 309] record_numbers: [2, 3, 4]
# pair_idx: 14 p_ids: [310, 311] record_numbers: [2, 3, 4]
# pair_idx: 15 p_ids: [400, 401] record_numbers: [2, 3]
# pair_idx: 16 p_ids: [402, 403] record_numbers: [6, 7]
# pair_idx: 17 p_ids: [404, 405] record_numbers: [2, 3]
# pair_idx: 18 p_ids: [406, 407] record_numbers: [3, 4]
# pair_idx: 19 p_ids: [408, 409] record_numbers: [2, 3]
# pair_idx: 20 p_ids: [410, 411] record_numbers: [2, 3]
# pair_idx: 21 p_ids: [412, 413] record_numbers: [2, 3]
# pair_idx: 22 p_ids: [414, 415] record_numbers: [2, 3]
# pair_idx: 23 p_ids: [416, 417] record_numbers: [3, 4]
# pair_idx: 24 p_ids: [418, 419] record_numbers: [2, 3]
# pair_idx: 25 p_ids: [422, 423] record_numbers: [2, 3]
# pair_idx: 26 p_ids: [424, 425] record_numbers: [2, 3]
# pair_idx: 27 p_ids: [426, 427] record_numbers: [2, 3]
# pair_idx: 28 p_ids: [428, 429] record_numbers: [2, 3]
# pair_idx: 29 p_ids: [430, 431] record_numbers: [2, 3]
# pair_idx: 30 p_ids: [432, 433] record_numbers: [2, 3]
# pair_idx: 31 p_ids: [434, 435] record_numbers: [2, 3]
# pair_idx: 32 p_ids: [436, 437] record_numbers: [2, 3]
# pair_idx: 33 p_ids: [438, 439] record_numbers: [2, 3]
# pair_idx: 34 p_ids: [440, 441] record_numbers: [2, 3]
# pair_idx: 35 p_ids: [442, 443] record_numbers: [3, 4]
# pair_idx: 36 p_ids: [444, 445] record_numbers: [2, 3]
# pair_idx: 37 p_ids: [446, 447] record_numbers: [2, 3]
# pair_idx: 38 p_ids: [448, 449] record_numbers: [3, 4]
# pair_idx: 39 p_ids: [450, 451] record_numbers: [2, 3]
# pair_idx: 40 p_ids: [452, 453] record_numbers: [2, 3]
# pair_idx: 41 p_ids: [454, 455] record_numbers: [3, 4]
# pair_idx: 42 p_ids: [456, 457] record_numbers: [2, 3]
# pair_idx: 43 p_ids: [458, 459] record_numbers: [2, 3]
# pair_idx: 44 p_ids: [460, 461] record_numbers: [2, 3]
# pair_idx: 45 p_ids: [462, 463] record_numbers: [2, 3]
# pair_idx: 46 p_ids: [464, 465] record_numbers: [2, 3]
# pair_idx: 47 p_ids: [466, 467] record_numbers: [2, 3]
# pair_idx: 48 p_ids: [468, 469] record_numbers: [2, 3]
# pair_idx: 49 p_ids: [470, 471] record_numbers: [3, 4]
# pair_idx: 50 p_ids: [472, 473] record_numbers: [4, 5]
# pair_idx: 51 p_ids: [474, 475] record_numbers: [2, 3]
# pair_idx: 52 p_ids: [476, 477] record_numbers: [2, 3]
# pair_idx: 53 p_ids: [478, 479] record_numbers: [2, 3]
# pair_idx: 54 p_ids: [480, 481] record_numbers: [2, 3]
# pair_idx: 55 p_ids: [482, 483] record_numbers: [2, 3]
# pair_idx: 56 p_ids: [484, 485] record_numbers: [3, 4]
# pair_idx: 57 p_ids: [486, 487] record_numbers: [2, 3]
# pair_idx: 58 p_ids: [488, 489] record_numbers: [2, 3]
# pair_idx: 59 p_ids: [490, 491] record_numbers: [2, 3]
# pair_idx: 60 p_ids: [492, 493] record_numbers: [2, 3]
# pair_idx: 61 p_ids: [494, 495] record_numbers: [2, 3]

# pair_idx: 0 p_ids: [100, 101] record_numbers: [3, 8]
# pair_idx: 1 p_ids: [102, 103] record_numbers: [3, 4, 5]
# pair_idx: 2 p_ids: [104, 105] record_numbers: [3, 4, 5]
# pair_idx: 3 p_ids: [200, 201] record_numbers: [4, 5, 7]
# pair_idx: 4 p_ids: [202, 203] record_numbers: [4, 5, 6]
# pair_idx: 5 p_ids: [204, 205] record_numbers: [2, 3, 4]
# pair_idx: 6 p_ids: [206, 207] record_numbers: [3, 5]
# pair_idx: 7 p_ids: [208, 209] record_numbers: [4, 6, 7]
# pair_idx: 8 p_ids: [210, 211] record_numbers: [8, 9]
# pair_idx: 9 p_ids: [300, 301] record_numbers: [4, 5, 6]
# pair_idx: 10 p_ids: [302, 303] record_numbers: [2, 3, 4]
# pair_idx: 11 p_ids: [304, 305] record_numbers: [3, 4, 5]
# pair_idx: 12 p_ids: [306, 307] record_numbers: [3, 4, 5]
# pair_idx: 13 p_ids: [308, 309] record_numbers: [2, 3, 4]
# pair_idx: 14 p_ids: [310, 311] record_numbers: [2, 3, 4]
# pair_idx: 15 p_ids: [400, 401] record_numbers: [2, 3]
# pair_idx: 16 p_ids: [402, 403] record_numbers: [6, 7]
# pair_idx: 17 p_ids: [404, 405] record_numbers: [2, 3]
# pair_idx: 18 p_ids: [406, 407] record_numbers: [3, 4]
# pair_idx: 19 p_ids: [408, 409] record_numbers: [2, 3]
# pair_idx: 20 p_ids: [410, 411] record_numbers: [2, 3]
# pair_idx: 21 p_ids: [412, 413] record_numbers: [2, 3]
# pair_idx: 22 p_ids: [414, 415] record_numbers: [2, 3]
# pair_idx: 23 p_ids: [416, 417] record_numbers: [3, 4]
# pair_idx: 24 p_ids: [418, 419] record_numbers: [2, 3]
# pair_idx: 25 p_ids: [422, 423] record_numbers: [2, 3]
# pair_idx: 26 p_ids: [424, 425] record_numbers: [2, 3]
# pair_idx: 27 p_ids: [426, 427] record_numbers: [2, 3]
# pair_idx: 28 p_ids: [428, 429] record_numbers: [2, 3]
# pair_idx: 29 p_ids: [430, 431] record_numbers: [2, 3]
# pair_idx: 30 p_ids: [432, 433] record_numbers: [2, 3]
# pair_idx: 31 p_ids: [434, 435] record_numbers: [2, 3]
# pair_idx: 32 p_ids: [436, 437] record_numbers: [2, 3]
# pair_idx: 33 p_ids: [438, 439] record_numbers: [2, 3]
# pair_idx: 34 p_ids: [440, 441] record_numbers: [2, 3]
# pair_idx: 35 p_ids: [442, 443] record_numbers: [3, 4]
# pair_idx: 36 p_ids: [444, 445] record_numbers: [2, 3]
# pair_idx: 37 p_ids: [446, 447] record_numbers: [2, 3]
# pair_idx: 38 p_ids: [448, 449] record_numbers: [3, 4]
# pair_idx: 39 p_ids: [450, 451] record_numbers: [2, 3]
# pair_idx: 40 p_ids: [452, 453] record_numbers: [2, 3]
# pair_idx: 41 p_ids: [454, 455] record_numbers: [3, 4]
# pair_idx: 42 p_ids: [456, 457] record_numbers: [2, 3]
# pair_idx: 43 p_ids: [458, 459] record_numbers: [2, 3]
# pair_idx: 44 p_ids: [460, 461] record_numbers: [2, 3]
# pair_idx: 45 p_ids: [462, 463] record_numbers: [2, 3]
# pair_idx: 46 p_ids: [464, 465] record_numbers: [2, 3]
# pair_idx: 47 p_ids: [466, 467] record_numbers: [2, 3]
# pair_idx: 48 p_ids: [468, 469] record_numbers: [2, 3]
# pair_idx: 49 p_ids: [470, 471] record_numbers: [3, 4]
# pair_idx: 50 p_ids: [472, 473] record_numbers: [4, 5]
# pair_idx: 51 p_ids: [474, 475] record_numbers: [2, 3]
# pair_idx: 52 p_ids: [476, 477] record_numbers: [2, 3]
# pair_idx: 53 p_ids: [478, 479] record_numbers: [2, 3]
# pair_idx: 54 p_ids: [480, 481] record_numbers: [2, 3]
# pair_idx: 55 p_ids: [482, 483] record_numbers: [2, 3]
# pair_idx: 56 p_ids: [484, 485] record_numbers: [3, 4]
# pair_idx: 57 p_ids: [486, 487] record_numbers: [2, 3]
# pair_idx: 58 p_ids: [488, 489] record_numbers: [2, 3]
# pair_idx: 59 p_ids: [490, 491] record_numbers: [2, 3]
# pair_idx: 60 p_ids: [492, 493] record_numbers: [2, 3]
# pair_idx: 61 p_ids: [494, 495] record_numbers: [2, 3]
