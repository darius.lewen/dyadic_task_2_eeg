import json
import os.path

import pandas as pd
import tables
from sched import scheduler as Scheduler
import time
from threading import Thread
import misc.conf as conf
import numpy as np

from backend.targets import CompTarget, CoopTarget, ChangingCoopTarget
from data_loader import pair_idx_to_ids, load_gaze, load_single, _gaze_data_exists, get_rec_num_path, \
    load_unpolished
from src.misc.loading_helper import polish_record


def _get_path(pair_idx, lap):
    player_ids, record_numbers = pair_idx_to_ids(pair_idx)
    player_ids, record_number = player_ids[lap], record_numbers[lap]
    p_ids = [str(player_ids[0]).zfill(3), str(player_ids[1]).zfill(3)]
    return f'../recordings/{p_ids[0]}_{p_ids[1]}/{record_number}'


def _get_trail_indices(pair_idx, lap):
    record = load_single(pair_idx, lap, 120)
    collections = record[[key for key in record.keys() if 'collected' in key]].to_numpy()
    collections = collections.sum(axis=1)
    return np.cumsum(collections)


# def _get_trail_indices(record):
#     collections = record[[key for key in record.keys() if 'collected' in key]].to_numpy()
#     collections = collections.sum(axis=1)
#     return np.cumsum(collections)


class ReplayEngine:
    def __init__(self, pair_idx, lap):  # todo refactor
        self._ticks_until_next_frame = 1
        self._slowed_tick_count = 0
        self._just_gone_backward = False
        #self._slowed = False
        #self._slowed_frame = False
        self._pair_idx = pair_idx
        self._lap = lap
        self._timestamp = 0
        self._start_date = None
        self._aims = [np.array([.5, .5]), np.array([.5, .5])]
        self._agents = [ReplayAgent() for _ in self._aims]
        self._targets = list()
        self._init_targets()
        self._scores = np.array([0, 0])
        self._scheduler = Scheduler(time.time, time.sleep)
        path = _get_path(pair_idx, lap)
        with tables.open_file(f'{path}/record.h5') as file:
            self._record = np.array(file.root.data)
        self._trail_indices = _get_trail_indices(pair_idx, lap)
        player_ids, record_numbers = pair_idx_to_ids(pair_idx)
        self._gazes = list()
        if _gaze_data_exists(player_ids[lap], record_numbers[lap], '../recordings'):
            gaze_p0, gaze_p1 = load_gaze(player_ids[lap], record_numbers[lap], '../recordings')
            self._gazes = [ReplayGaze(gaze_p0), ReplayGaze(gaze_p1)]
        self._current_frame = 0
        self._stopped = True
        self._ended = False
        self._trial_descriptions = None
        if os.path.exists(f'{path}/trial_descriptions.pkl'):
            self._trial_descriptions = pd.read_pickle(f'{path}/trial_descriptions.pkl')
        self._predictions = None
        if os.path.exists(f'{path}/prediction.npy'):
            self._predictions = np.load(f'{path}/prediction.npy')
        #if os.path.exists(f'{path}/trial_descriptions.json'):
        #    with open(f'{path}/trial_descriptions.json', 'r') as file:
        #        self._trial_descriptions = {int(trial_idx): trial_desc
        #                                    for trial_idx, trial_desc in json.load(file).items()}

    def _init_targets(self):
        args = (self._agents, lambda x, y: None, lambda: np.zeros(2))
        for _ in range(conf.competitive_targets):
            self._targets.append(CompTarget(*args))
        for _ in range(conf.cooperative_targets_type_0):
            self._targets.append(CoopTarget(0, *args))
        for _ in range(conf.cooperative_targets_type_1):
            self._targets.append(CoopTarget(1, *args))
        for _ in range(conf.cooperative_targets_type_2):  # T_ODO how to determine high_reward player at start? (unused)
            self._targets.append(ChangingCoopTarget(0, *args))
            
    def start(self):
        if self._start_date is None:
            self._start_date = time.time() + 1  # 1 sec delay util game starts
            for i in range(len(self._record)):
                self._scheduler.enterabs(self._start_date + i / conf.updates_ps, 1, self._tick)
            Thread(target=self._scheduler.run).start()

    def _load_next_frame(self):
        frame = self._record[self._current_frame]
        if not self._stopped:
            #self._slowed_frame = not self._slowed_frame
            #if self._slowed and self._slowed_frame:
            #    return
            #if self._ticks_until_next_frame != 1:
            self._slowed_tick_count += 1
            if self._slowed_tick_count % self._ticks_until_next_frame == 0:
                self._slowed_tick_count = 0
                self._current_frame += 1
        self._timestamp = frame[0, 2]
        self._aims[0] = frame[0, :2]
        self._aims[1] = frame[1, :2]
        self._agents[0].position = frame[2, :2]
        self._agents[1].position = frame[3, :2]
        self._scores = frame[2:4, 2]
        self._targets[0].position = frame[4, :2]
        self._targets[1].position = frame[5, :2]
        self._targets[2].position = frame[6, :2]

    def _tick(self):
        for gaze in self._gazes:
            gaze.tick(self._timestamp)
        self._load_next_frame()
        for e_target in self._targets:
            e_target.tick()

    def _end(self):  # TODO?
        pass

    def get_info(self):
        play_speed = 1 / self._ticks_until_next_frame
        trial_idx = self._trail_indices[self._current_frame + 1 if self._just_gone_backward else self._current_frame]
        return trial_idx, self._pair_idx, self._lap, play_speed, self._trial_descriptions, self._predictions

    def get_replay_controls(self):
        return {'forward': self.forward,
                'backward': self.backward,
                'start_stop': self.start_stop,
                'slow_down': self.slow_down,
                'speed_up': self.speed_up}

    def speed_up(self):
        self._change_play_speed(down=False)

    def slow_down(self):
        self._change_play_speed(down=True)

    def _change_play_speed(self, down):
        self._ticks_until_next_frame += 1 if down else -1
        if self._ticks_until_next_frame == 0:
            self._ticks_until_next_frame = 1

    def start_stop(self):
        self.start()
        self._stopped = not self._stopped

    def forward(self):
        self._just_gone_backward = False
        self._shift(forward=True)

    def backward(self):
        self._just_gone_backward = True
        self._shift(forward=False)

    def _shift(self, forward):
        self._stopped = True
        next_frame = self._current_frame
        last_trail_num = self._trail_indices[self._current_frame]
        while last_trail_num == self._trail_indices[next_frame]:
            next_frame += 1 if forward else -1
        #if not forward:
        #    next_frame += 1
        self._current_frame = next_frame

    @property
    def running(self):
        if self._start_date is not None:
            return self._start_date + 1 < time.time()
        return False

    @property
    def aims(self):
        return self._aims

    @property
    def agents(self):
        return self._agents

    @property
    def targets(self):
        return self._targets

    @property
    def scores(self):
        return list(self._scores)

    @property
    def ended(self):
        return self._ended

    @property
    def gazes(self):
        return self._gazes


class ReplayAgent:
    def __init__(self):
        self._position = np.zeros(2)

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, position):
        self._position = position


class ReplayGaze:
    def __init__(self, gaze_data):
        self._gaze = gaze_data[['x', 'y']].to_numpy()
        self._gaze_time = gaze_data['timestamp'].to_numpy()
        self._last_timestamp = 0

    def tick(self, timestamp):
        self._last_timestamp = timestamp

    def get_scaled_gaze(self):
        return tuple(self._gaze[np.argmin(abs(self._gaze_time - self._last_timestamp))])


