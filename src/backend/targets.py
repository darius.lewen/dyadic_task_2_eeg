from abc import ABC, abstractmethod

import numpy
import time
import misc.conf as conf

punishing_target_positions = numpy.array(conf.punishing_target_positions)


class Target(ABC):
    def __init__(self, agents, scored, get_valid_position):
        self._agents = agents
        self._scored = scored
        self._get_valid_position = get_valid_position
        self._position = numpy.array([.5, .5])
        self._replace()
        self._conquest_start_date = -1.

    def tick(self):
        if self._is_conquered():
            if self._was_conquered():
                self._yield_reward()
                self._conquest_start_date = -1.
                self._replace()

    @abstractmethod
    def _is_conquered(self):
        pass

    @abstractmethod
    def _yield_reward(self):
        pass

    @abstractmethod
    def id(self):
        pass

    @abstractmethod
    def fraction(self):
        pass

    def _was_conquered(self):
        if self._conquest_start_date > 0.:
            return self._conquest_start_date + conf.conquest_time < time.time()
        return False

    def _agent_on_target(self, agent_idx):
        dist_to_target = numpy.linalg.norm(self._position - self._agents[agent_idx].position)
        return dist_to_target < conf.target_radius #+ conf.agent_radius

    def _replace(self):
        self._position = self._get_valid_position()

    @property
    def position(self):
        return self._position

    @position.setter   # For replay
    def position(self, position):
        self._position = position

    @property
    def occupation_progress(self):
        if self._conquest_start_date < 0:
            return 0
        return 1 - (self._conquest_start_date + conf.conquest_time - time.time()) / conf.conquest_time


class CompTarget(Target):

    def __init__(self, agents, *args, **kwargs):
        super().__init__(agents, *args, **kwargs)
        self._occupying_agent = None

    def _on_comp_target_entered(self, agent_idx):
        ###############################
        #  todo add eeg triggers here
        ###############################
        # print(f'Comp entered by player {agent_idx}')
        pass

    def _on_comp_target_occupied(self, agent_idx):
        ###############################
        #  todo add eeg triggers here
        ###############################
        # print(f'Comp occupied by player {agent_idx}')
        pass

    def _yield_reward(self):
        self._on_comp_target_occupied(self._occupying_agent)
        self._scored([self._occupying_agent], [conf.comp_score])  # agent occupied

    def _is_conquered(self):
        if self._occupying_agent is None:
            for agent_idx in [0, 1]:
                if self._agent_on_target(agent_idx):
                    self._occupying_agent = agent_idx  # agent entered
                    self._on_comp_target_entered(agent_idx)
                    self._conquest_start_date = time.time()
                    return True
            return False
        elif not self._agent_on_target(self._occupying_agent):
            self._occupying_agent = None  # agent left or occupied
            self._conquest_start_date = -1.
            return False
        return True  # agent still on target

    def fraction(self):
        return conf.comp_score / conf.max_target_score

    def id(self):
        return 2

    @property
    def occupying_agent(self):
        return self._occupying_agent


class PunishingTarget(CompTarget):

    def __init__(self, is_valid_pos, *args, **kwargs):
        self._is_valid_pos = is_valid_pos
        self._pos_idx = int(numpy.random.random() * len(punishing_target_positions))
        self._occupying_agent = None
        super().__init__(*args, **kwargs)

    def _yield_reward(self):
        self._scored([(1 + self._occupying_agent) % 2], [conf.punishing_score])  # agent occupied

    def fraction(self):  # todo
        return conf.comp_score / conf.max_target_score

    def id(self):
        return 3

    @property
    def occupying_agent(self):
        return self._occupying_agent

    def _replace(self):
        free_positions = [pos for pos in punishing_target_positions if self._is_valid_pos(pos)]
        if len(free_positions) > 0:
            next_pos_idx = int(numpy.random.random() * len(free_positions))
            self._position = free_positions[next_pos_idx]


class CoopTarget(Target):
    def __init__(self, high_player_idx, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._high_player_idx = high_player_idx
        self._p_already_on_t = [False, False]

    def _on_coop_target_entered(self, coop_t_idx, p_idx):
        ###############################
        #  todo add eeg triggers here
        ###############################
        # print(f'Coop{coop_t_idx} entered by player {p_idx}')
        pass

    def _on_coop_target_occupied(self, coop_t_idx):
        ###############################
        #  todo add eeg triggers here
        ###############################
        # print(f'Coop{coop_t_idx} occupied')
        pass

    def _yield_reward(self):
        self._scored([(self._high_player_idx + 1) % 2, self._high_player_idx],
                     [conf.coop_score_low, conf.coop_score_high])
        self._on_coop_target_occupied(self._high_player_idx)

    def _handle_entered_triggers(self):
        for p in [0, 1]:
            if self._agent_on_target(p):
                if not self._p_already_on_t[p]:
                    self._on_coop_target_entered(self._high_player_idx, p_idx=p)
                    self._p_already_on_t[p] = True
            else:
                self._p_already_on_t[p] = False

    def _is_conquered(self, ):
        is_conquered = self._agent_on_target(0) and self._agent_on_target(1)  # occupation starts

        self._handle_entered_triggers()

        if self._conquest_start_date < 0 and is_conquered:
            self._conquest_start_date = time.time()
        if self._conquest_start_date > 0 and not is_conquered:
            self._conquest_start_date = -1.

        return is_conquered

    @property
    def high_player_idx(self):
        return self._high_player_idx

    def fraction(self):
        return conf.coop_score_high / conf.max_target_score

    def id(self):
        return self._high_player_idx


class ChangingCoopTarget(CoopTarget):
    def _yield_reward(self):
        super()._yield_reward()
        self._high_player_idx += 1
        self._high_player_idx %= 2

