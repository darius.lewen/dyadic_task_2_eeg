import time
import random
import numpy
from sched import scheduler as Scheduler
from threading import Thread

import misc.conf as conf
from ui.input.aim import MouseAim, TouchAim
from backend.agent import Agent
from backend.targets import CompTarget, CoopTarget, ChangingCoopTarget, PunishingTarget
from backend.gaze.time_stamper import TimeStamper


class Engine:
    def __init__(self, depositor, after_game):
        self._after_game = after_game
        self._start_date = None
        self._aims = [MouseAim(self), MouseAim(self)] if not conf.touch else [TouchAim(self), TouchAim(self)]
        self._agents = [Agent(aim) for aim in self._aims]
        self._targets = []
        self._init_targets()
        self._scores = numpy.array([0, 0])
        self._timestamp = numpy.array([0])
        self._game = numpy.zeros((4 + len(self._targets), 3))
        self._update_game_matrix()
        self._depositor = depositor
        self._depositor.init_record_file(self._game.shape)
        self._scheduler = Scheduler(time.time, time.sleep)
        self._time_stamper = TimeStamper()
        self._first_timestamp = None
        self._last_timestamp = None
        self._ended = False
        self._target_collections = 0
        self._schedules_event_ids = list()
        self._current_frame = 0
        # pyglet.clock.schedule(self._on_engine_end)

    def _on_engine_end(self):
        # if self.ended:
        # pyglet.clock.unschedule(self._on_engine_end)
        for id in self._schedules_event_ids[self._current_frame+1:]:
            self._scheduler.cancel(id)
        self._after_game()

    def _init_targets(self):
        for _ in range(conf.competitive_targets):
            self._targets.append(CompTarget(self._agents, self._scored, self._get_valid_position))
        for _ in range(conf.cooperative_targets_type_0):
            self._targets.append(CoopTarget(0, self._agents, self._scored, self._get_valid_position))
        for _ in range(conf.cooperative_targets_type_1):
            self._targets.append(CoopTarget(1, self._agents, self._scored, self._get_valid_position))
        for _ in range(conf.cooperative_targets_type_2):  # TODO how to determine high_reward player at start?
            self._targets.append(ChangingCoopTarget(0, self._agents, self._scored, self._get_valid_position))
        for _ in range(conf.punishing_targets):
            self._targets.append(PunishingTarget(self._is_valid, self._agents, self._scored, self._get_valid_position))

    def _is_valid(self, position):
        for target in self._targets:
            dist = numpy.linalg.norm(target.position - position)
            if dist < conf.target_radius * 2:
                return False
        return True

    def _get_valid_position(self, position=None):
        if position is not None:
            return self._is_valid(position)
        position = numpy.array([random.random(), random.random()])
        if self._is_valid(position):
            return position
        return self._get_valid_position()

    def _scored(self, p_indices, scores_made):
        for p_idx, score in zip(p_indices, scores_made):
            self._scores[p_idx] += score
        self._target_collections += 1

    def start(self):
        if self._start_date is None and not self._ended:
            self._start_date = time.time() + 1  # 1 sec delay until game starts
            for i in range(conf.updates_total):
                self._schedules_event_ids.append(self._scheduler.enterabs(self._start_date + i / conf.updates_ps,
                                                                          1, self._tick))
            end_time = self._start_date + conf.updates_total / conf.updates_ps
            self._schedules_event_ids.append(self._scheduler.enterabs(end_time, 1, self._on_engine_end))
            Thread(target=self._scheduler.run).start()

    def _update_game_matrix(self):
        self._game[0, :2] = self._aims[0].position
        self._game[1, :2] = self._aims[1].position
        self._game[0, 2] = self._timestamp
        self._game[2, :2] = self._agents[0].position
        self._game[3, :2] = self._agents[1].position
        self._game[2:4, 2] = self._scores
        for i, target in enumerate(self._targets):
            self._game[4+i, :2] = target.position
            self._game[4+i, 2] = target.id()

    def _tick(self):
        if self.ended:  # this will be currently only executed if max_target count, otherwise the last tick toggles ended
            self._on_engine_end()
        else:
            self._current_frame += 1
            self._timestamp = self._time_stamper.get_timestamp()
            if self._first_timestamp is None:
                self._first_timestamp = self._timestamp
                ###############################
                #  todo add eeg triggers here - the block started
                ###############################
            for agent in self._agents:
                agent.tick()
            for target in self._targets:
                target.tick()
            self._update_game_matrix()
            self._depositor.deposit(self._game)
            if conf.updates_total == self._depositor.get_length() or conf.max_target_collections == self._target_collections:
                print(conf.updates_total == self._depositor.get_length())
                self._end()

    def _end(self):
        self._depositor.deposit_gaze(prefix='main', start_time=self._first_timestamp, end_time=self._timestamp)
        self._depositor.deposit_misc(self._start_date)
        self._start_date = None
        self._ended = True
        ###############################
        #  todo add eeg triggers here - the block ended
        ###############################

    @property
    def ended(self):
        return self._ended

    @property
    def first_timestamp(self):
        return self._first_timestamp

    @property
    def last_timestamp(self):
        return self._last_timestamp

    @property
    def running(self):
        if self._start_date is not None:
            return self._start_date + 1 < time.time()
        return False

    @property
    def game(self):
        return self._game

    @property
    def aims(self):
        return self._aims

    @property
    def agents(self):
        return self._agents

    @property
    def targets(self):
        return self._targets

    @property
    def scores(self):
        return list(self._scores)
