import time
import zmq
import misc.conf as conf


class TimeStamper:
    def __init__(self):
        if conf.eye_tracking:
            zmq_context = zmq.Context()
            self._pupil_remote = zmq_context.socket(zmq.REQ)
            self._pupil_remote.connect(f'tcp://{conf.ip_0}:{conf.port_0}')

    def get_timestamp(self):
        if not conf.eye_tracking:
            return time.time()
        else:
            self._pupil_remote.send_string('t')
            return float(self._pupil_remote.recv_string())
