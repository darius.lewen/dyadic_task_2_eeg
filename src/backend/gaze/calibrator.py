import pyglet.app
from backend.agent import Agent
from backend.gaze.time_stamper import TimeStamper
import misc.conf as conf
import numpy


class Calibrator:
    def __init__(self):
        self._target = CalibrationTarget()
        self._agent = Agent(self._target, conf.calibration_speed)
        self._pupil_remote = None
        self._time_stamper = TimeStamper()
        self._game_data = []
        self._after_calibration = None
        self._running = False

    def tick(self, dt):
        if self._running:
            if self._target.finished:
                self._close()
            self._game_data.append([*self._agent.position, self._time_stamper.get_timestamp()])
            self._update_target()

    def start(self):
        self._running = True

    def _close(self):
        pyglet.clock.unschedule(self.tick)
        self._after_calibration(self.game_data)

    def _update_target(self):
        self._agent.tick()
        if (self._agent.position == self._target.position).all():
            self._target.next()

    @property
    def game_data(self):
        return self._game_data

    @property
    def agent(self):
        return self._agent

    @property
    def after_calibration(self):
        return self._after_calibration

    @after_calibration.setter
    def after_calibration(self, after_calibration):
        self._after_calibration = after_calibration


class CalibrationTarget:
    def __init__(self):
        self._target_position_idx = 0
        self.finished = False

    def next(self):
        if self._target_position_idx == len(conf.target_positions) - 1:
            self.finished = True
        else:
            self._target_position_idx += 1

    @property
    def position(self):
        return numpy.array(conf.target_positions[self._target_position_idx])
