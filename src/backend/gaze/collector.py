from threading import Thread
from queue import LifoQueue
import misc.conf as conf
import zmq
import msgpack
import numpy


class Collector:
    def __init__(self, ip, port, surface, scaling_func=None):
        self._scaling_func = scaling_func
        self._message_queue = LifoQueue()
        self._surface_subscription = None
        self._connect(ip, port, surface)
        self._keep_listen = True
        Thread(target=self._listen).start()

    def stop(self):
        self._keep_listen = False

    @property
    def scaling_func(self):
        return self._scaling_func

    @scaling_func.setter
    def scaling_func(self, func):
        self._scaling_func = func

    def _newest_message(self):
        # this could mess up the message order due to race condition
        # fixed in return of drain_gaze_data()
        message = self._message_queue.get()
        self._message_queue.put(message)
        return message

    def get_current_gaze(self):
        glances = self._newest_message()['gaze_on_surfaces']
        confident_glances = [gaze for gaze in glances
                             if gaze['confidence'] > conf.gaze_confidence_threshold]
        if len(confident_glances) == 0:
            return -1000, -1000
        gaze = sum(([numpy.array(list(gaze['norm_pos'])) for gaze in confident_glances]))/len(confident_glances)
        return tuple(gaze)

    def get_scaled_gaze(self):
        return self._scaling_func(*(self.get_current_gaze()))

    def drain_gaze_data(self, start_time, end_time):
        _gaze_data = []
        while not self._message_queue.empty():
            message = self._message_queue.get()
            for gaze in message['gaze_on_surfaces']:
                if start_time < gaze['timestamp'] < end_time:
                    _gaze_data.append([*gaze['norm_pos'], gaze['timestamp'], gaze['confidence']])
        return sorted(_gaze_data, key=lambda x: x[2])

    def _connect(self, ip, port, surface):
        self._zmq_context = zmq.Context()
        self._pupil_remote = self._zmq_context.socket(zmq.REQ)
        self._pupil_remote.connect(f'tcp://{ip}:{port}')
        self._pupil_remote.send_string('SUB_PORT')
        _sub_port = self._pupil_remote.recv_string()
        self._pupil_remote.send_string('PUB_PORT')
        self._surface_subscription = self._zmq_context.socket(zmq.SUB)
        self._surface_subscription.connect(f'tcp://{ip}:{_sub_port}')
        self._surface_subscription.subscribe(f'surfaces.{surface}')
        print(f'Established connection to {ip}')

    def _listen(self):
        while self._keep_listen:
            topic, payload = self._surface_subscription.recv_multipart()
            self._message_queue.put(msgpack.loads(payload))
