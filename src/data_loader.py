import numpy as np
import tables
import pandas
import json
import os.path

from src.misc.loading_helper import record_catalogue, unique_pairs, pairs, polish_record


recordings_folder = '../recordings'


def load(pair_idx, frames_per_sec):
    """
    Function to load a list() of pandas.DataFrame objects, each contains 20 minutes of recorded data.

    DataFrame column naming:
    p0 = player 0 = blue player/agent
    p1 = player 1 = orange player/agent
    comp = competitive target = white target
    coop0 = cooperative target with higher share for player 0 = predominantly blue target
    coop1 = cooperative target with higher share for player 1 = predominantly orange target

    :param pair_idx: int. index of the pair which records will be loaded.
    :param frames_per_sec: int. resolution of the DataFrames. Maximal value is 120.
    :return: list of DataFrames, one DataFrame for each 20-minute block. Mostly three DataFrames, sometimes two.
    """
    records = load_records(*pair_idx_to_ids(pair_idx),
                           recordings_folder,
                           bin_multiplier=120//frames_per_sec)['recs']
    return [polish_record(rec) for rec in records]


def load_unpolished(pair_idx, lap, frames_per_sec):  # backwards compatibility... todo!
    player_ids, record_numbers = pair_idx_to_ids(pair_idx)
    return load_record(player_ids[lap], record_numbers[lap], recordings_folder, bin_multiplier=120//frames_per_sec)[0]


def load_single(pair_idx, lap, frames_per_sec):
    return polish_record(load_unpolished(pair_idx, lap, frames_per_sec))


def print_pair_idx_to_ids():
    for pair_idx in range(len(unique_pairs)):
        player_ids, record_numbers = pair_idx_to_ids(pair_idx)
        print(f'pair_idx: {pair_idx} p_ids: {player_ids[0]} record_numbers: {record_numbers}')


def pair_idx_to_ids(pair_idx):
    pair = unique_pairs[pair_idx]
    indices = list()
    for pair_idx, other_pair in enumerate(pairs):
        if other_pair == pair:
            indices.append(pair_idx)
    player_ids = [record_catalogue['player_ids'][i] for i in indices]
    record_numbers = [record_catalogue['record_numbers'][i] for i in indices]
    return player_ids, record_numbers


def load_records(player_ids, record_numbers, recordings_folder, bin_multiplier=1):
    tmp = [load_record(*arg, recordings_folder, bin_multiplier) for arg in zip(player_ids, record_numbers)]
    return {'recs': [x[0] for x in tmp],
            'target_prefixes': tmp[0][1]}


def load_record(player_ids, record_number, recordings_folder, bin_multiplier):
    path = get_rec_num_path(player_ids, record_number, recordings_folder)
    if not os.path.exists(path + '/target_prefixes.json'):
        _gen_and_save_rec(player_ids, record_number, recordings_folder)
    record, target_prefixes = _load_processed_record(path)
    if bin_multiplier == 1:
        return record, target_prefixes
    record = _downscale(record.to_dict(orient='list'), bin_size=bin_multiplier)
    return pandas.DataFrame(record), target_prefixes


def _target_type_to_str(target_type):
    if target_type == 0:
        return 'coop_p0'
    if target_type == 1:
        return 'coop_p1'
    if target_type == 2:
        return 'comp'
    if target_type == 3:
        return 'punish'


def _float_equals(a, b, precision=10 * 6):
    return int(a * precision) == int(b * precision)


def _target_moved(prev_pos, pos):
    return not (_float_equals(prev_pos[0], pos[0]) and _float_equals(prev_pos[1], pos[1]))


def _scored_at_idx(pref, record_dict, idx):
    if pref.startswith('comp'):
        if record_dict[f'p0_score'][idx] != record_dict[f'p0_score'][idx - 1]:
            return 0
        else:
            return 1
    elif pref.startswith('punish'):
        if record_dict[f'p0_score'][idx] != record_dict[f'p0_score'][idx - 1]:
            return 1
        else:
            return 0


def _add_target_occupations(record_dict, target_prefixes):
    for target_pref in target_prefixes:
        if target_pref.startswith('comp') or target_pref.startswith('punish'):
            _add_comp_occ(target_pref, record_dict)
        else:
            _add_coop_occ(target_pref, record_dict)


def _add_comp_occ(pref, record_dict):
    p0_occupations = _get_zeros(record_dict)
    p1_occupations = _get_zeros(record_dict)
    target_positions = _get_positions(pref, record_dict)
    prev_pos = target_positions[0]
    for i, pos in enumerate(target_positions):
        if _target_moved(prev_pos, pos):
            if _scored_at_idx(pref, record_dict, i) == 0:
                p0_occupations[i] = 1
            else:
                p1_occupations[i] = 1
        prev_pos = pos
    record_dict[f'{pref}_occupations_of_p0'] = p0_occupations
    record_dict[f'{pref}_occupations_of_p1'] = p1_occupations


def _add_coop_occ(pref, record_dict):
    occupations = [0 for _ in range(len(record_dict['timestamp']))]
    target_positions = list(zip(record_dict[f'{pref}_x'], record_dict[f'{pref}_y']))
    prev_pos = target_positions[0]
    for i, pos in enumerate(target_positions):
        if _target_moved(prev_pos, pos):
            occupations[i] = 1
        prev_pos = pos
    record_dict[pref + '_occupations'] = occupations


def _agent_on_target(agent, target, conf):
    agent = np.array(list(agent))
    target = np.array(list(target))
    distance = np.linalg.norm(agent - target)
    return distance < conf['target_radius']


def _add_target_entries_exits(record_dict, target_prefixes, conf):
    for target_pref in target_prefixes:
        target_positions = _get_positions(target_pref, record_dict)
        for p_idx in [0, 1]:
            entries = _get_zeros(record_dict)
            exits = _get_zeros(record_dict)
            agent_positions = _get_positions(f'p{p_idx}_agent', record_dict)
            on_target_before = False
            for i, (agent, target) in enumerate(zip(agent_positions, target_positions)):
                if not on_target_before and _agent_on_target(agent, target, conf):
                    entries[i] = 1
                    on_target_before = True
                elif on_target_before and not _agent_on_target(agent, target, conf):
                    if not _target_occupied(target_pref, i, record_dict):
                        exits[i] = 1
                    on_target_before = False
            record_dict[f'{target_pref}_entries_of_p{p_idx}'] = entries
            record_dict[f'{target_pref}_exits_of_p{p_idx}'] = exits


def _get_positions(pref, record_dict):
    return list(zip(record_dict[f'{pref}_x'], record_dict[f'{pref}_y']))


def _get_zeros(record_dict):
    return [0 for _ in range(len(record_dict['timestamp']))]


def _target_occupied(pref, idx, record_dict):
    if pref.startswith('coop') and record_dict[f'{pref}_occupations'][idx] == 1:
        return True
    elif pref.startswith('comp') or pref.startswith('punish'):
        if record_dict[f'{pref}_occupations_of_p0'][idx] == 1 or record_dict[f'{pref}_occupations_of_p1'][idx] == 1:
            return True
    else:
        return False


def get_target_amount(conf):
    amount = conf['competitive_targets'] + conf['cooperative_targets_type_0'] + conf['cooperative_targets_type_1']
    if 'punishing_targets' in conf.keys():
        amount += conf['punishing_targets']
    return amount


def _get_target_pref_and_pos(data, conf):
    type_counters = [0, 0, 0, 0]  # one counter for each target type
    target_amount = get_target_amount(conf)
    target_prefixes = []
    target_x_s = []
    target_y_s = []
    for i in range(len(data[0]) - target_amount, len(data[0])):
        target_data = data[:, i]
        target_type = int(target_data[0, 2])
        target_pref = f'{_target_type_to_str(target_type)}_{type_counters[target_type]}'
        type_counters[target_type] += 1
        target_prefixes.append(target_pref)
        target_x_s.append(target_data[:, 0])
        target_y_s.append(target_data[:, 1])
    return target_prefixes, target_x_s, target_y_s


def _add_target_data(record_dict, raw_data, conf, path):
    target_data_path = path + '/target_data.json'
    target_prefixes, target_x_s, target_y_s = _get_target_pref_and_pos(raw_data, conf)
    if os.path.exists(target_data_path):
        return _load_target_data(target_data_path, target_prefixes, record_dict)
    _gen_target_data(record_dict, target_prefixes, conf, target_x_s, target_y_s)
    _save_target_data(record_dict, target_prefixes, target_data_path)
    return target_prefixes


def _gen_target_data(record_dict, target_prefixes, conf, target_x_s, target_y_s):
    for pref, x, y in zip(target_prefixes, target_x_s, target_y_s):
        record_dict[pref + '_x'] = x
        record_dict[pref + '_y'] = y
    _add_target_occupations(record_dict, target_prefixes)
    _add_target_entries_exits(record_dict, target_prefixes, conf)


def _load_target_data(target_data_path, target_prefixes, record_dict):
    with open(target_data_path) as file:
        for key, value in json.load(file).items():
            record_dict[key] = value
        return target_prefixes


def _save_target_data(record_dict, target_prefixes, path):
    target_data = dict()
    for key, value in record_dict.items():
        for pref in target_prefixes:
            if key.startswith(pref):
                target_data[key] = list(value)
                continue
    with open(path, 'w') as file:
        json.dump(target_data, file)


def _add_gazes_on_target(record_dict, threshold=.3):  # TODO to hyperparameter
    for p_idx in [0, 1]:
        for t_name in ['comp_0', 'coop_p0_0', 'coop_p1_0']:
            target_pos = record_dict[f'{t_name}_x'], record_dict[f'{t_name}_y']
            gaze_pos = record_dict[f'p{p_idx}_gaze_x'], record_dict[f'p{p_idx}_gaze_y']
            target_pos = np.array(list(target_pos))
            gaze_pos = np.array(list(gaze_pos))
            gaze_agent_dist = np.linalg.norm(target_pos - gaze_pos, axis=0)
            out = np.zeros(len(gaze_agent_dist), dtype=int)
            out[gaze_agent_dist < threshold] = 1
            record_dict[f'p{p_idx}_gazes_on_{t_name}'] = out


def _add_empty_gaze_data(record_dict, n):
    nones = [None for _ in range(n)]
    for key in ['p0_gaze_x', 'p0_gaze_y', 'p1_gaze_x', 'p1_gaze_y']:
        record_dict[key] = nones
    for p_idx in [0, 1]:
        for t_name in ['comp_0', 'coop_p0_0', 'coop_p1_0']:
            record_dict[f'p{p_idx}_gazes_on_{t_name}'] = nones


def _add_gaze_data(record_dict, player_ids, record_number, recordings_folder):
    if _gaze_data_exists(player_ids, record_number, recordings_folder):
        gaze_p0, gaze_p1 = load_gaze(player_ids, record_number, recordings_folder)
        gaze_p0 = _compress_gaze(gaze_p0, record_dict['timestamp'])
        gaze_p1 = _compress_gaze(gaze_p1, record_dict['timestamp'])
        gaze_p0 = _add_key_prefix(gaze_p0, prefix='p0_gaze_')
        gaze_p1 = _add_key_prefix(gaze_p1, prefix='p1_gaze_')
        record_dict |= gaze_p0
        record_dict |= gaze_p1
        _add_gazes_on_target(record_dict)
    else:
        _add_empty_gaze_data(record_dict, len(record_dict['timestamp']))


def _add_key_prefix(a, prefix):
    return {prefix + key: val for key, val in a.items()}


def _compress_gaze(gaze, timestamps):
    gaze = gaze[gaze['confidence'] > .8]  # TODO to argument
    out = {'x': list(), 'y': list()}
    last_x, last_y = 0., 0.
    gaze_idx = 0
    for ts in timestamps:
        _gaze = gaze.iloc[gaze_idx:]
        gazes_for_ts = _gaze[_gaze['timestamp'] <= ts]
        gaze_for_ts = gazes_for_ts.mean()
        x, y = gaze_for_ts['x'], gaze_for_ts['y']
        if np.isnan(x) or np.isnan(y):
            x, y = last_x, last_y
        else:
            last_x, last_y = x, y
        out['x'].append(x)
        out['y'].append(y)
        gaze_idx += len(gazes_for_ts)
    out['x'] = np.array(out['x'])
    out['y'] = np.array(out['y'])
    return out


def _save_rec(target_prefixes, record_dict, path):
    pandas.DataFrame(record_dict).to_pickle(path + '/processed_record.pkl')
    with open(path + '/target_prefixes.json', 'w') as file:
        json.dump(target_prefixes, file)


def _load_processed_record(path):
    record = pandas.read_pickle(path + '/processed_record.pkl')
    with open(path + '/target_prefixes.json') as file:
        target_prefixes = json.load(file)
    return record, target_prefixes


def _gaze_data_exists(player_ids, record_number, recordings_folder):
    path = get_rec_num_path(player_ids, record_number, recordings_folder) + f'/pre_gaze.json'
    file_empty = os.stat(path).st_size == 0
    return not file_empty


def _gen_and_save_rec(player_ids, record_number, recordings_folder):
    conf = _get_json('conf', player_ids, record_number, recordings_folder)
    #p_ids = [str(player_ids[0]).zfill(3), str(player_ids[1]).zfill(3)]
    #path = f'{recordings_folder}/{p_ids[0]}_{p_ids[1]}/{record_number}'
    path = get_rec_num_path(player_ids, record_number, recordings_folder)
    raw_data = _get_raw_data(path)
    _record_dict = _get_base_record_dict(raw_data)
    target_prefixes = _add_target_data(_record_dict, raw_data, conf, path)
    print(target_prefixes)
    _add_gaze_data(_record_dict, player_ids, record_number, recordings_folder)
    _save_rec(target_prefixes, _record_dict, path)


def _downscaled_nones(n):
    return np.array([None for _ in range(n)])


def _downscale(_record_dict, bin_size):
    if bin_size == 1:
        return _record_dict
    bin_slice = [(i*bin_size, (i+1)*bin_size) for i in range(len(_record_dict['timestamp']) // bin_size)]
    out = dict()
    for key, ts in _record_dict.items():
        if None in ts:
            out[key] = _downscaled_nones(len(bin_slice))
        elif key in ['timestamp', 'p0_score', 'p1_score']:
            out[key] = _downscale_asc_int(ts, bin_slice)
        elif 'aim' in key or 'agent' in key or 'gaze_' in key:
            out[key] = _downscale_float(ts, bin_slice)
        elif isinstance(_record_dict[key][0], float) or isinstance(_record_dict[key][0], np.float64):
            out[key] = _downscale_target_pos(ts, bin_slice)
        else:
            out[key] = _downscale_binary(ts, bin_slice)
    return out


def _downscale_target_pos(ts, bin_slice):
    return np.array([ts[end - 1] for _, end in bin_slice[:-1]] + [ts[-1]])


def _downscale_asc_int(ts, bin_slice):
    return np.array([ts[start + (end-start) // 2] for start, end in bin_slice])


def _downscale_float(ts, bin_slice):
    return np.array([np.mean(ts[start: end]) for start, end in bin_slice])


def _downscale_binary(ts, bin_slice):
    return np.array([1 if 1 in ts[start: end] else 0 for start, end in bin_slice])


def _get_raw_data(path):
    with tables.open_file(f'{path}/record.h5') as file:
        return np.array(file.root.data)


def _get_base_record_dict(raw_data):
    return {
        'timestamp': raw_data[:, 0, 2],
        'p0_aim_x': raw_data[:, 0, 0],
        'p0_aim_y': raw_data[:, 0, 1],
        'p1_aim_x': raw_data[:, 1, 0],
        'p1_aim_y': raw_data[:, 1, 1],
        'p0_agent_x': raw_data[:, 2, 0],
        'p0_agent_y': raw_data[:, 2, 1],
        'p1_agent_x': raw_data[:, 3, 0],
        'p1_agent_y': raw_data[:, 3, 1],
        'p0_score': raw_data[:, 2, 2],
        'p1_score': raw_data[:, 3, 2],
    }


def _gaze_list_to_dict(gaze, scaling_func):
    out = {
        'timestamp': list(map(lambda x: x[2], gaze)),
        'x_raw': list(map(lambda x: x[0], gaze)),
        'y_raw': list(map(lambda x: x[1], gaze)),
        'confidence': list(map(lambda x: x[3], gaze)),
    }
    scaled = [scaling_func(x, y) for x, y in zip(out['x_raw'], out['y_raw'])]
    out = {
        **out,
        'x': list(map(lambda x: x[0], scaled)),
        'y': list(map(lambda x: x[1], scaled))
    }
    return out


def get_rec_num_path(player_ids, record_number, recordings_folder):
    p_ids = [str(player_ids[0]).zfill(3), str(player_ids[1]).zfill(3)]
    return f'{recordings_folder}/{p_ids[0]}_{p_ids[1]}/{record_number}'


def load_gaze(player_ids, record_number, recordings_folder, type_prefix='main'):
    path = get_rec_num_path(player_ids, record_number, recordings_folder) + f'/{type_prefix}_gaze.json'
    with open(path) as file:
        json_file = json.load(file)
        gaze_p0 = json_file['p0_gaze']
        gaze_p1 = json_file['p1_gaze']
        scale_gaze_p0, scale_gaze_p1 = _load_gaze_scaling(player_ids, record_number, recordings_folder)
        gaze_p0 = pandas.DataFrame(_gaze_list_to_dict(gaze_p0, scale_gaze_p0))
        gaze_p1 = pandas.DataFrame(_gaze_list_to_dict(gaze_p1, scale_gaze_p1))
        return gaze_p0, gaze_p1


def _get_json(name, player_ids, record_number, recordings_folder):
    rec_path = get_rec_num_path(player_ids, record_number, recordings_folder)
    with open(rec_path + f'/{name}.json') as file:
        return json.load(file)


def _load_gaze_scaling(player_ids, record_number, recordings_folder):
    misc = _get_json('misc', player_ids, record_number, recordings_folder)
    p0_poly_x = misc['p0_poly_x']
    p0_poly_y = misc['p0_poly_y']
    p1_poly_x = misc['p1_poly_x']
    p1_poly_y = misc['p1_poly_y']

    def scale_gaze_p0(x, y):
        x = (x - p0_poly_x[1]) / p0_poly_x[0]
        y = (y - p0_poly_y[1]) / p0_poly_y[0]
        return x, y

    def scale_gaze_p1(x, y):
        x = (x - p1_poly_x[1]) / p1_poly_x[0]
        y = (y - p1_poly_y[1]) / p1_poly_y[0]
        return x, y

    return scale_gaze_p0, scale_gaze_p1


if __name__ == '__main__':
    # Example on how to load record together with raw gaze data
    # type_prefix='pre': gaze data from gaze calibration process before block
    # type_prefix='main': gaze data from block
    # type_prefix='post': gaze data from gaze calibration process after block
    pair_idx, lap = 0, 0
    # p_ids, record_numbers = pair_idx_to_ids(pair_idx)
    # gaze_p0, gaze_p1 = load_gaze(p_ids[lap], record_numbers[lap], recordings_folder, type_prefix='main')
    rec = load(pair_idx, frames_per_sec=5)[lap]

