import pyglet
import misc.conf as conf
import backend.gaze.scaling as gaze_scaling
from ui.calibration_ui import CalibrationUI
from ui.ui import UI
from ui.ui import windows
from backend.engine import Engine
from backend.depositor import Depositor
from backend.gaze.calibrator import Calibrator
from backend.gaze.collector import Collector

ui_0, ui_1 = None, None
gaze_collectors = None
depositor = None


def calibrate(after_cal):
    calibrator = Calibrator()
    _ = CalibrationUI(windows[0], calibrator.agent, start_func=calibrator.start)
    _ = CalibrationUI(windows[1], calibrator.agent, start_func=calibrator.start)
    pyglet.clock.schedule_interval(calibrator.tick, 1 / conf.updates_ps)
    calibrator.after_calibration = after_cal


def after_pre_calibration(game_data):
    depositor.deposit_gaze('pre', start_time=game_data[0][2], end_time=game_data[-1][2])
    gaze_data_0, gaze_data_1 = depositor.load_gaze('pre')
    scaling_func_0, model_0 = gaze_scaling.get_gaze_scaling_func(gaze_data_0, game_data)
    scaling_func_1, model_1 = gaze_scaling.get_gaze_scaling_func(gaze_data_1, game_data)
    gaze_collectors[0].scaling_func = scaling_func_0
    gaze_collectors[1].scaling_func = scaling_func_1
    depositor.models = [model_0, model_1]
    start_game(depositor, after_game=lambda: calibrate(after_post_calibration))


def start_game(depositor=None, after_game=None):
    if depositor is None:
        depositor = Depositor()
    if after_game is None:
        after_game = lambda: final_end(depositor)
    engine = Engine(depositor, after_game)
    global ui_0, ui_1
    ui_0 = UI(engine, windows[1])
    ui_1 = UI(engine, windows[0], depositor.gaze_collectors)


def after_post_calibration(game_data):
    depositor.deposit_gaze('post', start_time=game_data[0][2], end_time=game_data[-1][2])
    final_end(depositor)


def final_end(depositor):
    depositor.close()
    pyglet.app.exit()
    print('Successful end')


if __name__ == '__main__':
    if not conf.eye_tracking:
        start_game()
    else:
        print('Connect to pupil core...')
        gaze_collectors = [Collector(ip=conf.ip_0, port=conf.port_0, surface=conf.surface_name_0),
                           Collector(ip=conf.ip_1, port=conf.port_1, surface=conf.surface_name_1)]
        print('Success!')
        depositor = Depositor(gaze_collectors)
        calibrate(after_pre_calibration)
    pyglet.app.run()

