from pyglet.window import key
import pyglet

screens = pyglet.canvas.get_display().get_screens()


class ButtonController:
    def __init__(self, window, start_engine):
        self._window = window
        self._start_engine = start_engine
        self._window.push_handlers(self.on_key_press)

    def on_key_press(self, symbol, modifiers):
        if symbol == key.SPACE:
            self._start_engine()


class ReplayButtonController:
    def __init__(self, window, forward, backward, start_stop, slow_down, speed_up):
        self._window = window
        self._forward = forward
        self._backward = backward
        self._start_stop = start_stop
        self._slow_down = slow_down
        self._speed_up = speed_up
        self._window.push_handlers(self.on_key_press)

    def on_key_press(self, symbol, modifiers):
        if symbol == key.SPACE:
            self._start_stop()
        elif symbol == key.A or symbol == key.LEFT:
            self._backward()
        elif symbol == key.D or symbol == key.RIGHT:
            self._forward()
        elif symbol == key.S or symbol == key.DOWN:
            self._slow_down()
        elif symbol == key.W or symbol == key.UP:
            self._speed_up()

