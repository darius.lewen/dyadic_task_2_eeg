import pyglet
import numpy as np
from pyglet.shapes import BorderedRectangle, Circle
from pyglet.graphics import Batch, OrderedGroup

import misc.conf as conf
import misc.color as color
import src.misc.utils as utils
import backend.targets as backend_targets
from pyglet.sprite import Sprite
from ui.output.agent import Agent
from ui.output.aim import Aim
from ui.output.targets import CompTarget, CoopTarget, PunishingTarget
from ui.output.score import Score
from ui.input.buttons import ReplayButtonController, ButtonController

from src.ui.output.replay_info import ReplayInfo
from src.ui.output.replay_pred_distri import PredictionDistribution

_all_screens = pyglet.canvas.get_display().get_screens()
_screens = [_all_screens[i] for i in conf.screens]
windows = [pyglet.window.Window(screen=screen, fullscreen=True, vsync=True) for screen in _screens]


class UI:
    def __init__(self, engine, window=None, gaze_collectors=None, replay=False):
        if window is None:
            window = windows[0]
        self._replay = replay
        self._window = window
        self._window.set_mouse_visible(False)
        self._window.event(self.on_draw)
        if not replay:
            self._button_controller = ButtonController(self._window, engine.start)
        else:
            self._button_controller = ReplayButtonController(self._window, **engine.get_replay_controls())
        self._engine = engine
        self._gaze_collectors = gaze_collectors

        self._batch = Batch()
        self._groups = [OrderedGroup(i) for i in range(9)]

        self._background = None
        self._agents = []
        self._aims = []
        self._targets = []
        self._glances = []
        self._scores = []
        self._replay_info = None
        self._init_elements()
        pyglet.clock.schedule(self._tick)

    def _init_elements(self):
        self._background = BorderedRectangle(conf.field_x_offset, conf.field_y_offset,
                                             conf.field_size, conf.field_size,
                                             color=color.background,
                                             border_color=color.field_border,
                                             border=3,
                                             batch=self._batch,
                                             group=self._groups[0])
        target_args = dict(batch=self._batch,
                           groups=self._groups[1:3])
        for e_target in self._engine.targets:
            if isinstance(e_target, backend_targets.PunishingTarget):
                self._targets.append(PunishingTarget(e_target, **target_args))
            elif isinstance(e_target, backend_targets.CompTarget):
                self._targets.append(CompTarget(e_target, **target_args))
            elif isinstance(e_target, backend_targets.CoopTarget):
                self._targets.append(CoopTarget(e_target, **target_args))
        for i in [0, 1]:
            self._agents.append(Agent(p_color=color.players[i],
                                      batch=self._batch,
                                      groups=self._groups[4:6],))
            self._aims.append(Aim(player_idx=i,
                                  batch=self._batch,
                                  group=self._groups[6]))
            self._scores.append(Score(player_idx=(i + 1) % 2,
                                      batch=self._batch,
                                      group=self._groups[7]))
        if self._gaze_collectors is not None:
            for i, _ in enumerate(self._gaze_collectors):
                self._glances.append(Circle(0, 0, conf.gaze_radius * conf.field_size,
                                            batch=self._batch, group=self._groups[8], color=color.gaze[i]))
        if self._replay:
            self._replay_info = ReplayInfo(batch=self._batch, group=self._groups[7])
            if conf.show_prediction:
                self._pred_distri = PredictionDistribution(batch=self._batch, group=self._groups[7])

    def _tick(self, dx):
        if self._engine.ended:
            print('ui thread unschedules itself...')
            pyglet.clock.unschedule(self._tick)
            return
        if self._engine.running:
            for i, e_aim in enumerate(self._engine.aims):
                self._aims[i].position = utils.to_win_pos(e_aim)
            for i, e_agent in enumerate(self._engine.agents):
                self._agents[i].position = utils.to_win_pos(e_agent)
            for i, e_target in enumerate(self._engine.targets):
                self._targets[i].position = utils.to_win_pos(e_target)
                self._targets[i].tick()
        if conf.show_both_rewards:
            for i, score in enumerate(self._engine.scores):
                self._scores[i].score = self._engine.scores
        else:
            for i, score in enumerate(self._engine.scores):
                self._scores[i].score = score

        if self._gaze_collectors is not None:
            for i, collector in enumerate(self._gaze_collectors):
                scaled_gaze = collector.get_scaled_gaze()
                self._glances[i].position = utils.to_win_pos(scaled_gaze)
        if self._replay:
            replay_info = self._engine.get_info()
            self._replay_info.set_info(*replay_info)
            if conf.show_prediction:
                trial_idx = replay_info[0]
                predictions = replay_info[-1]
                prediction_idx = trial_idx - 4
                if prediction_idx < 0:
                    self._pred_distri.update(np.array([0, 0, 0]))
                else:
                    self._pred_distri.update(predictions[prediction_idx])

    def on_draw(self):
        if not self._engine.ended:  # todo why this if statement??
            self._window.clear()
            self._batch.draw()

