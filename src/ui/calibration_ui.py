from pyglet.graphics import Batch, OrderedGroup
from pyglet.shapes import BorderedRectangle
import misc.conf as conf
import misc.color as color
import misc.utils as utils

from ui.output.agent import Agent
from ui.input.buttons import ButtonController


class CalibrationUI:
    def __init__(self, window, target_agent, start_func=lambda: None):
        self._target_agent = target_agent
        self._window = window
        self._button_controller = ButtonController(self._window,
                                                   start_engine=start_func)
        self._batch = Batch()
        self._groups = [OrderedGroup(i) for i in range(3)]
        self._background = BorderedRectangle(conf.field_x_offset, conf.field_y_offset,
                                             conf.field_size, conf.field_size,
                                             color=color.background,
                                             border_color=color.field_border,
                                             border=3,
                                             batch=self._batch,
                                             group=self._groups[0])
        self._agent = Agent(color.calibration_agent,
                            batch=self._batch,
                            groups=self._groups[1:3])
        self._window.event(self.on_draw)

    def on_draw(self):
        self._agent.position = utils.to_win_pos(self._target_agent)
        self._window.clear()
        self._batch.draw()
