import numpy as np
from pyglet.sprite import Sprite
import misc.conf as conf
from misc.create_images import get_score_image


class Score(Sprite):
    def __init__(self, player_idx, *args, **kwargs):
        if conf.show_both_rewards :
            self._score = np.array([0, 0])
        else:
            self._score = 0
        self._player_idx = player_idx

        if conf.show_both_rewards:
            super().__init__(img=get_score_image(np.array([0, 0]), both_rewards=True, mirror=self._player_idx == 0,
                                                 flip=self._player_idx == 0 and conf.invert_y), *args, **kwargs)
            if player_idx == 0:  # Quick adjustment to horizontal screen
                self.position = 0 - conf.score_x_offset, \
                                conf.field_y_offset - self.height - conf.score_y_offset
            else:
                self.position = 1920 - self.width + conf.score_x_offset, \
                                conf.field_y_offset - self.height - conf.score_y_offset

        else:
            super().__init__(img=get_score_image(0, player=self._player_idx, both_rewards=False, mirror=self._player_idx == 0,
                                                 flip=self._player_idx == 0 and conf.invert_y), *args, **kwargs)
            if conf.invert_y:
                if player_idx == 0:
                    self.position = 540 - (self.width // 2) - conf.score_x_offset, \
                                    1920 - conf.score_y_offset
                else:
                    self.position = 540 - (self.width // 2) + conf.score_x_offset, \
                                    conf.score_y_offset - self.height
            else:
                if player_idx == 0:
                    self.position = 0 - conf.score_x_offset, \
                                    conf.field_y_offset - self.height - conf.score_y_offset
                else:
                    self.position = 1080 - self.width + conf.score_x_offset, \
                                    conf.field_y_offset - self.height - conf.score_y_offset

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, score):
        if conf.show_both_rewards:
            if score[0] != self._score[0] or score[1] != self._score[1]:
                self._score = score
                if conf.invert_y:
                    self.image = get_score_image(score, mirror=self._player_idx == 0, flip=self._player_idx == 0,
                                                 both_rewards=conf.show_both_rewards)
                else:
                    self.image = get_score_image(score, mirror=self._player_idx == 0, both_rewards=conf.show_both_rewards)
        else:
            if score != self._score:
                self._score = score
                if conf.invert_y:
                    self.image = get_score_image(score, mirror=self._player_idx == 0, flip=self._player_idx == 0,
                                                 player=self._player_idx)
                else:
                    self.image = get_score_image(score, mirror=self._player_idx == 0, player=self._player_idx)


