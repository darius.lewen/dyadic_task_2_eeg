import numpy as np

from pyglet.sprite import Sprite

from src.misc.create_images import get_replay_info_image
import src.misc.conf as conf


def prob_vec_to_str(prob):
    return ''.join([str(p * 100)[:2] + '% ' for p in prob])


def get_info_row(value, name):
    name = name + ':'
    value = value if isinstance(value, str) else "%.2f" % value
    return name.ljust(15) + value.rjust(16)


def entropy(x):
    return -1 * np.sum(x * np.log2(x))


class ReplayInfo(Sprite):
    def __init__(self, *args, **kwargs):
        super().__init__(img=get_replay_info_image(['', '', '']), *args, **kwargs)
        self.position = (0, 1080 - self.height)

    def set_info(self, trial_num, pair_idx, lap, play_speed, td, predictions, number_width=4):
        info_str_rows = [
            # f'Replay speed:              {f"{int(play_speed * 100)}%".rjust(number_width)}',
            # f'Pair:                      {str(pair_idx).rjust(number_width)}',
            # f'Lap:                       {str(lap).rjust(number_width)}',
            f'Trial:                     {str(trial_num).rjust(number_width)}'
        ]
        if td is not None:
            trial_desc = td.iloc[trial_num]
            #
            # coll_type = trial_desc['coll_type']
            # effort = trial_desc['chosen_effort']
            # trajectory_distance = trial_desc['distance']
            # extra_dist = trial_desc['effort_ol']
            # miscoordination_measure = trial_desc['mis_cord_measure']
            # correlation = trial_desc['correlation']
            # ahead_measure = trial_desc['ahead_measure']
            # dist_ol = trial_desc['distance_ol']
            trial_class = trial_desc['trial_class']
            #
            # # prediction = trial_desc['prediction3']
            #
            # info_str_rows.append(get_info_row(coll_type, 'coll_type'))
            # info_str_rows.append(get_info_row(effort, 'effort'))
            # info_str_rows.append(get_info_row(trajectory_distance, 'distance'))
            # info_str_rows.append('')
            # info_str_rows.append(get_info_row(extra_dist, 'effort ol'))
            # info_str_rows.append(get_info_row(dist_ol, 'dist ol'))
            # info_str_rows.append('')
            # info_str_rows.append(get_info_row(miscoordination_measure, 'miscoord'))
            # info_str_rows.append(get_info_row(correlation, 'correlation'))
            # info_str_rows.append(get_info_row(ahead_measure, 'ahead_measure'))
            info_str_rows.append(get_info_row(trial_class, 'trial class'))
            # info_str_rows.append('')
            if conf.show_prediction:
                entropy_str = str(None)
                if predictions is not None:
                    prediction_idx = trial_num - 4
                    if prediction_idx >= 0:
                        current_prediction = predictions[prediction_idx]
                        entropy_str = get_info_row(entropy(current_prediction), 'Entropy')
                info_str_rows.append(entropy_str)

        self.image = get_replay_info_image(info_str_rows)
        self.position = (0, 1080 - self.height)

