import time

import pandas as pd

record_catalogue = {
    'player_ids': [
        [0, 2],
    ],
    'record_numbers': [19]
}

# record_catalogue = {
#     'player_ids': [
#
#         # [50, 51],
#         # [50, 51],
#
#         [100, 101],
#         [100, 101],
#
#         [102, 103],
#         [102, 103],
#         [102, 103],
#
#         [104, 105],
#         [104, 105],
#         [104, 105],
#
#         [200, 201],
#         [200, 201],
#         [200, 201],
#
#         [202, 203],
#         [202, 203],
#         [202, 203],
#
#         [204, 205],
#         [204, 205],
#         [204, 205],
#
#         [206, 207],
#         #[206, 207], 206_207 - 4 is corrupted
#         [206, 207],
#
#         [208, 209],
#         [208, 209],
#         [208, 209],
#
#         [210, 211],
#         [210, 211],
#
#         [300, 301],
#         [300, 301],
#         [300, 301],
#
#         [302, 303],
#         [302, 303],
#         [302, 303],
#
#         [304, 305],
#         [304, 305],
#         [304, 305],
#
#         [306, 307],
#         [306, 307],
#         [306, 307],
#
#         [308, 309],
#         [308, 309],
#         [308, 309],
#
#         [310, 311],
#         [310, 311],
#         [310, 311],  # 14
#     ],
#     'record_numbers': [#4, 5,
#                        3, 8, 3, 4, 5, 3, 4, 5, 4, 5, 7, 4, 5, 6, 2, 3, 4, 3, 5, 4, 6, 7,
#                        8, 9,
#                        4, 5, 6, 2, 3, 4, 3, 4, 5,
#                        3, 4, 5, 2, 3, 4, 2, 3, 4]
# }
#
# record_catalogue_2 = {
#     'player_ids': [
#         [400, 401],
#         [400, 401],
#
#         # [402, 403],  # excluded due to non-normal behaviour
#         # [402, 403],
#
#         [404, 405],
#         [404, 405],
#
#         [406, 407],
#         [406, 407],
#
#         [408, 409],
#         [408, 409],
#
#         [410, 411],
#         [410, 411],
#
#         [412, 413],
#         [412, 413],
#
#         [414, 415],
#         [414, 415],
#
#         [416, 417],
#         [416, 417],
#
#         [418, 419],
#         [418, 419],
#
#         [422, 423],
#         [422, 423],
#
#         [424, 425],
#         [424, 425],
#
#         [426, 427],
#         [426, 427],
#     ],
#     'record_numbers': [2, 3,
#                        # 6, 7, # excluded due to non-normal behaviour
#                        # 2, 3, 4,
#                        2, 3,
#                        3, 4,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        3, 4,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        ]
# }
#
# record_catalogue_3 = {
#     'player_ids': [
#         [428, 429],
#         [428, 429],
#
#         [430, 431],
#         [430, 431],
#
#         [432, 433],
#         [432, 433],
#
#         [434, 435],
#         [434, 435],
#
#         [436, 437],
#         [436, 437],
#
#         [438, 439],
#         [438, 439],
#
#         [440, 441],
#         [440, 441],
#
#         [442, 443],
#         [442, 443],
#
#         [444, 445],
#         [444, 445],
#
#         [446, 447],
#         [446, 447],
#
#         [448, 449],
#         [448, 449],
#
#         [450, 451],
#         [450, 451],
#
#         # [452, 453],  # excluded due to non-normal behaviour
#         # [452, 453],
#
#         [454, 455],
#         [454, 455],
#
#         [456, 457],
#         [456, 457],
#
#         # [458, 459],  # excluded due to non-normal behaviour
#         # [458, 459],
#
#         [460, 461],
#         [460, 461],
#
#         [462, 463],
#         [462, 463],
#
#         [464, 465],
#         [464, 465],
#
#         [466, 467],
#         [466, 467],
#
#         [468, 469],
#         [468, 469],
#
#         [470, 471],
#         [470, 471],
#
#         [472, 473],
#         [472, 473],
#
#         [474, 475],
#         [474, 475],
#
#         [476, 477],
#         [476, 477],
#
#         [478, 479],
#         [478, 479],
#
#         [480, 481],
#         [480, 481],
#
#         [482, 483],
#         [482, 483],
#
#         # [484, 485],  # excluded due to non-normal behaviour
#         # [484, 485],
#
#         [486, 487],
#         [486, 487],
#
#         [488, 489],
#         [488, 489],
#
#         [490, 491],
#         [490, 491],
#
#         [492, 493],
#         [492, 493],
#
#         [494, 495],
#         [494, 495],
#
#     ],
#     'record_numbers': [2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        3, 4,  # 442, 443
#                        2, 3,
#                        2, 3,
#                        3, 4,  # 448, 449
#                        2, 3,
#                        # 2, 3, # excluded due to non-normal behaviour
#                        3, 4,  # 454, 455
#                        2, 3,
#                        # 2, 3, # excluded due to non-normal behaviour
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        3, 4,  # 470, 471
#                        4, 5,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        #3, 4,  # 484 # excluded due to non-normal behaviour
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        2, 3,
#                        ]
# }
#
# record_catalogue = {key: val + record_catalogue_2[key] + record_catalogue_3[key]
#                     for key, val in record_catalogue.items()}

pairs = [tuple(id_pair) for id_pair in record_catalogue['player_ids']]
unique_pairs = list(sorted(set(pairs)))


def polish_record(record):
    org_timestamp_column = record['timestamp']
    record['timestamp'] = record['timestamp'].to_numpy() - record['timestamp'].iloc[0]
    keys = ['timestamp',
            'p0_score',
            'p1_score',
            'p0_agent_x',
            'p0_agent_y',
            'p1_agent_x',
            'p1_agent_y',
            'p0_gaze_x',
            'p0_gaze_y',
            'p1_gaze_x',
            'p1_gaze_y',
            'comp_0_x',
            'comp_0_y',
            'punish_0_x',
            'punish_0_y',
            'coop_p0_0_x',
            'coop_p0_0_y',
            'coop_p1_0_x',
            'coop_p1_0_y',
            'comp_0_occupations_of_p0',
            'comp_0_occupations_of_p1',
            'punish_0_occupations_of_p0',
            'punish_0_occupations_of_p1',
            'coop_p0_0_occupations',
            'coop_p1_0_occupations',
            'comp_0_entries_of_p0',
            'comp_0_entries_of_p1',
            'punish_0_entries_of_p0',
            'punish_0_entries_of_p1',
            'coop_p0_0_entries_of_p0',
            'coop_p0_0_entries_of_p1',
            'coop_p1_0_entries_of_p0',
            'coop_p1_0_entries_of_p1',
            'comp_0_exits_of_p0',
            'comp_0_exits_of_p1',
            'punish_0_exits_of_p0',
            'punish_0_exits_of_p1',
            'coop_p0_0_exits_of_p0',
            'coop_p0_0_exits_of_p1',
            'coop_p1_0_exits_of_p0',
            'coop_p1_0_exits_of_p1',
            ]
    new_keys = ['timestamp',
                'p0_score',
                'p1_score',
                'p0_agent_x',
                'p0_agent_y',
                'p1_agent_x',
                'p1_agent_y',
                'p0_gaze_x',
                'p0_gaze_y',
                'p1_gaze_x',
                'p1_gaze_y',
                'comp_x',
                'comp_y',
                'punish_x',
                'punish_y',
                'coop0_x',
                'coop0_y',
                'coop1_x',
                'coop1_y',
                'comp_collected_by_p0',
                'comp_collected_by_p1',
                'punish_collected_by_p0',
                'punish_collected_by_p1',
                'coop0_collected',
                'coop1_collected',
                'comp_entered_by_p0',
                'comp_entered_by_p1',
                'punish_entered_by_p0',
                'punish_entered_by_p1',
                'coop0_entered_by_p0',
                'coop0_entered_by_p1',
                'coop1_entered_by_p0',
                'coop1_entered_by_p1',
                'comp_left_by_p0',
                'comp_left_by_p1',
                'punish_left_by_p0',
                'punish_left_by_p1',
                'coop0_left_by_p0',
                'coop0_left_by_p1',
                'coop1_left_by_p0',
                'coop1_left_by_p1',
                ]
    polished_record = pd.DataFrame({new_key: record[key] for key, new_key in zip(keys, new_keys)
                                    if key in record.keys()})
    polished_record['org_timestamp'] = org_timestamp_column
    return polished_record


