# debug ================================================================================================================
eye_tracking = False
touch = False

# player ids ===========================================================================================================
p0_id = 0
p1_id = 2
# each participant should get a unique id


# game =================================================================================================================
mouse_sensitivity = 0.00015
# Factor in front of each mice movement value.

max_speed = 0.007
# Speed limit of agent in percent of filed_width. If one exceeds it, one looses, as a penalty, track of mouse position.

conquest_time = 1
# Time one needs to hover over a target until it is occupied.

updates_ps = 120
# Amount of updates per second, affects how smooth the game runs and is also bin size of recorded data.

max_target_collections = 6
# End recoding after max_target_selections

# recording_time = 6 * 1  # 6s for debug
recording_time = 60 * 1  # warm-up
# recording_time = 60 * 20  # full record
# In seconds.

updates_total = updates_ps * recording_time


# scoring ==============================================================================================================
comp_score = 7
# Score for competitive target. Should be <= max_target_score.

coop_score_low = 2
# Low score for cooperative target. Sum of high and low should equal max_target_score.

coop_score_high = 5
# High score for cooperative target. Sum of high and low should equal max_target_score.

max_target_score = 7
# Total score which a target can yield

punishing_score = -21

score_to_euro_factor = .01
# translation factor for reward visualization.


# target amounts =======================================================================================================
competitive_targets = 1

cooperative_targets_type_0 = 1
# type 0 is beneficial for player 0

cooperative_targets_type_1 = 1
# type 1 is beneficial for player 1

cooperative_targets_type_2 = 0   # CAUTION: Loading records with this target type is not implemented!
# type 2 switches between type 0 and 1 after each occupation

# punishing_targets = 1
punishing_targets = 0

# # Trial comp
# competitive_targets = 1
# cooperative_targets_type_0 = 0
# cooperative_targets_type_1 = 0
# cooperative_targets_type_2 = 0

# # Trial coop
# competitive_targets = 0
# cooperative_targets_type_0 = 1
# cooperative_targets_type_1 = 1
# cooperative_targets_type_2 = 0

# shapes - relative (in percent of field_width) ========================================================================
target_radius = .05  # target and agent sizes determine the game
agent_radius = .02
aim_radius = .0  # aim and gaze are only for visualization
# gaze_radius = .01
gaze_radius = .0
punishing_target_positions = [[0, 0], [0, 1], [1, 0], [1, 1]]

show_prediction = True

# shapes - absolute ====================================================================================================
screen_width = 1920
screen_height = 1080
field_size = 800
field_x_offset = screen_width // 2 - field_size // 2
field_y_offset = screen_height - field_size - 50

if touch:
    field_size = 620  # for touch prototype
    field_x_offset = screen_width // 2 - field_size // 2
    field_y_offset = screen_height - field_size - 40
    rel_field_width = screen_height / screen_width - 2 * target_radius
    rel_field_height = 1 - 2 * target_radius
    rel_field_x_offset = (1 - rel_field_width) / 2
    rel_field_y_offset = (1 - rel_field_height) / 2



score_width = 1920 // 2
score_height = 100
score_x_offset = field_size // 2 + 140
score_y_offset = -120
font = 'DejaVuSansMono.ttf'
replay_info_width = 1920 // 2
replay_info_height = 40
font_punishing_target = 'TimesNewRoman.ttf'

# reward display =======================================================================================================

show_both_rewards = True
invert_y = False  # (score visualization)


# devices ==============================================================================================================
# Use misc.utils.print_devices() to see your available mice devices and screen indices.
invert_second_mouse = True

# mice = ['Logitech M280/320/275',
#         'Logitech M280/320/275']  # 2nd is inverted
mice = ['Logitech G502 HERO Gaming Mouse',
        'TPPS/2 IBM TrackPoint']  # 2nd is inverted
screens = [0,
           1]


# gaze calibration =====================================================================================================
gaze_confidence_threshold = 0.6
calibration_speed = 0.0075
target_positions = [[0.5, 0.5], [0, 0], [0, 1], [1, 0], [1, 1], [0, 1], [0, 0], [1, 0], [0, 1], [1, 1], [1, 0], [0, 0],
                    [1, 0], [0.5, 0.5], [1, 0.5], [0.5, 1], [0, 0.5], [0.5, 0], [0.5, 1], [1, 0.5], [0, 0.5],
                    [0.5, 0.5], [1, 1]]
target_positions = [[0.5, 0.5], [0, 0]]


# network ==============================================================================================================
ip_0 = '172.17.3.148'
port_0 = 50020
surface_name_0 = 'Surface1'

# ip_1 = '172.16.10.244'
ip_1 = '172.17.3.208'
port_1 = 50020
surface_name_1 = 'Surface1'


