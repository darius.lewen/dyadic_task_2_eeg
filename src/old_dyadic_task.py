import pyglet
import misc.conf as conf
import backend.gaze.scaling as gaze_scaling
from ui.calibration_ui import CalibrationUI
from ui.ui import UI
from backend.engine import Engine
from backend.depositor import Depositor
from backend.gaze.calibrator import Calibrator
from backend.gaze.collector import Collector

ui_0, ui_1 = None, None
calibrator_2, calibration_ui = None, None


#  Todo del this file


def dyadic_task():
    collector_0 = Collector(ip=conf.ip_0,
                            port=conf.port_0,
                            surface=conf.surface_name_0)
    collector_1 = Collector(ip=conf.ip_1,
                            port=conf.port_1,
                            surface=conf.surface_name_1)
    depositor = Depositor(gaze_collectors=[collector_0, collector_1])
    calibrator = Calibrator()
    global calibration_ui
    calibration_ui = CalibrationUI(calibrator.agent, start_func=calibrator.start)
    print('established connection to pupil core')
    pyglet.clock.schedule_interval(calibrator.tick, 1 / conf.updates_ps)
    pyglet.clock.schedule(calibration_ui.tick)

    def after_post_calibration():
        game_data = calibrator_2.game_data  # data[i] = [x, y, timestamp]
        depositor.deposit_gaze('post', start_time=game_data[0][2], end_time=game_data[-1][2])
        gaze_data_0, gaze_data_1 = depositor.load_gaze('post')
        scaling_func_0, model_0 = gaze_scaling.get_gaze_scaling_func(gaze_data_0, game_data)
        scaling_func_1, model_1 = gaze_scaling.get_gaze_scaling_func(gaze_data_1, game_data)
        depositor.close()
        pyglet.app.exit()

    def after_game():
        global ui_0, ui_1
        ui_0.unschedule()
        ui_1.unschedule()
        pyglet.app.exit()
        ui_0._window.unschedule()
        ui_1._window.unschedule()
        global calibrator_2, calibration_ui
        calibrator_2 = Calibrator()
        calibration_ui = CalibrationUI(calibrator_2.agent, start_func=calibrator_2.start)
        calibrator_2.after_calibration = after_post_calibration
        pyglet.clock.schedule_interval(calibrator_2.tick, 1 / conf.updates_ps)
        pyglet.clock.schedule(calibration_ui.tick)
        pyglet.app.run()

    def after_calibration():

        game_data = calibrator.game_data  # data[i] = [x, y, timestamp]
        depositor.deposit_gaze('pre', start_time=game_data[0][2], end_time=game_data[-1][2])

        gaze_data_0, gaze_data_1 = depositor.load_gaze('pre')
        scaling_func_0, model_0 = gaze_scaling.get_gaze_scaling_func(gaze_data_0, game_data)
        scaling_func_1, model_1 = gaze_scaling.get_gaze_scaling_func(gaze_data_1, game_data)
        collector_0.scaling_func = scaling_func_0
        collector_1.scaling_func = scaling_func_1

        depositor.models = [model_0, model_1]

        engine = Engine(depositor, after_game)
        global ui_0, ui_1
        ui_0 = UI(engine)
        ui_1 = UI(engine, [collector_0, collector_1])

        pyglet.clock.unschedule(calibrator.tick)
        pyglet.clock.unschedule(calibration_ui.tick)
        calibration_ui.close()

    calibrator.after_calibration = after_calibration

    pyglet.app.run()


def print_devices():
    print('\nSelect mice form here:')
    for device in pyglet.input.evdev.get_devices():
        print(f'\'{device.name}\'')
    print('\nSelect screen indices form these:')
    for i, screen in enumerate(pyglet.canvas.get_display().get_screens()):
        print(f'index={i} x={screen.x} y={screen.y}')


if __name__ == '__main__':
    print_devices()
    if not conf.eye_tracking:
        engine = Engine(depositor=Depositor(),
                        after_game=pyglet.app.exit)
        ui_0 = UI(engine)
        ui_1 = UI(engine)
        pyglet.app.run()
    else:
        dyadic_task()

