Dyadic Task
=========

![til](./res/vid.gif)
Our goal in designing this task is to create a continuous foraging game in which dyadic interactions and social behavior emerge. Individuals (blue and orange) collect targets (the large circles) by hovering their respective agents (the small circles) over them. To ensure a wide variety of interactions, we use three different targets. A white, competitive target that is collected by whoever reaches it first, and two blue-orange, cooperative targets that are collected when both individuals hover over them simultaneously. The collector of the competitive target receives 7 cents, while for the cooperative targets there is a low (2 cents) and a high (5 cents) prize, depending on the individual's color share.

Supported package und python versions
--------
| Package      | Version |
|--------------|---------|
| Python       | 2.10    |
| matplotlib   | 3.8.3   |
| msgpack      | 1.0.7   |
| numpy        | 1.26.4  |
| pandas       | 2.2.1   |
| pyglet       | 1.5.27  |
| pyftdi       | 0.55.0  |
| pyzmq        | 25.1.2  |
| scikit-learn | 1.4.0   |
| scipy        | 1.12.0  |
| tables       | 3.9.2   |


Load Data
--------
The following Python packages are required to load the data:

    numpy, pandas, tables

You can either run data_loader.py or import load() from data_loader.py. After running data_loader.py, one finds a variable records in the namespace, which is a list of lists of pandas.DataFrames. Each of these DataFrames contains a 20-minute block of recoded data. The functionality of the load() function is as follows:

    def load(pair_idx, frames_per_sec):
        """
        Function to load the recorded data for a specific player pair as pandas DataFrames. Column naming:

        p0 = player 0 = blue player/agent
        p1 = player 1 = orange player/agent
        comp = competitive target = white target
        coop0 = cooperative target with higher share for player 0 = predominantly blue target
        coop1 = cooperative target with higher share for player 1 = predominantly orange target

        :param pair_idx: int. index of the pair which records will be loaded.
        :param frames_per_sec: int. resolution of the DataFrames. Maximal value is 120.
        :return: list of DataFrames, one DataFrame for each 20-minute block. Mostly three DataFrames, sometimes two.
        """


Replay 
--------
To replay the recorded games, run the replay.py, this requires the following Python packages:

    numpy, pandas, tables, pyglet

One can select the data to be replayed by editing the file itself, simply by replacing the pair_idx and lap arguments with the appropriate arguments. Possible values for pair_idx are in the range(8), and there are usually three and sometimes two laps available.
